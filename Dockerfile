FROM quay.io/fedora/fedora:38

ENV VIRTCTL_VERSION="v1.0.0"

RUN cd /usr/bin && \
	arch=$(arch | sed s/aarch64/arm64/ | sed s/x86_64/amd64/) && \
	curl -L https://github.com/sonaproject/virtctl/raw/master/${VIRTCTL_VERSION}/virtctl-${VIRTCTL_VERSION}-linux-${arch} --output virtctl && \
	chmod 755 virtctl && \
	curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/${arch}/kubectl" && \
	chmod 755 kubectl && \
	mkdir /manifests

COPY kubevirt-console-logger.yaml /manifests/kubevirt-console-logger.yaml
COPY entrypoint.sh /usr/bin/entrypoint.sh
COPY vm-console-dbug-launcher.sh /usr/bin/vm-console-dbug-launcher.sh

ENTRYPOINT ["/usr/bin/entrypoint.sh"]

